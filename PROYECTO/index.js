const express = require('express');
const basicAuth = require('express-basic-auth');
const {autorización} = require('./middlewares/usuarioexistente.middleware');
const {EsAdministrador} = require('./middlewares/Administrador.middleware');
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');

const pedidoRoutes = require('./routes/pedidos.routes');
const usuarioRoutes = require('./routes/usuarios.routes');
const productosRoutes = require('./routes/productos.routes');
const app = express();
app.use(express.json());

const swaggerOptions = require('./utils/swagger');

const swaggerSpecs = swaggerJsDoc(swaggerOptions);

app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerSpecs))

app.use('/login', usuarioRoutes);

app.use('/usuarios', usuarioRoutes);

app.use(basicAuth({ authorizer: autorización }));

app.use('/productos', productosRoutes);

app.use('/pedidos', pedidoRoutes)


app.listen(5000, () => { console.log('Escuchando en el puerto 5000') });
