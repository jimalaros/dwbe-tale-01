const express = require('express');
const app = express();
const basicAuth = require('express-basic-auth')
const autenticación = require('./middlewares/autenticación.middleware')

app.use(express.json());


app.get('/login', (req, res) => {
    res.json('sin autenticacion');
});

app.post('/registrarUsuario', (req, res) => {
    res.json('sin autenticacion');
});

app.use(basicAuth({ authorizer: autenticación }));

const usuarioRoutes = require('./routes/usuario.routes');

app.use('/usuarios', usuarioRoutes);

const productoRoutes = require('./routes/producto.routes');

app.use('/productos', productoRoutes);

const pedidoRoutes = require('./routes/pedido.route');

app.use('/pedidos', pedidoRoutes)

app.listen(5000, () => { console.log('Escuchando en el puerto 5000') });
