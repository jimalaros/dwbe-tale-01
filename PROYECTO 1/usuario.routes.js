const express = require('express');
const { each } = require('underscore');
const router = express.Router();
const _ = require('underscore');

const users = require('../usuarios.json')

router.get('/', (req, res) => {
    res.json(users);
});

/*router.get('/Login', (req, res) => {
    const { correo, contraseña } = req.body;
    if(correo)
        if(contraseña)
        {
            res.json(`El usuario ${correo} ha iniciado sesión correctamente`)
        }
        else{
            res.json(`El usuario ${correo} no ha iniciado sesión correctamente`)
        }
    else{
        res.json(`El usuario ${correo} no existe`)
    }
});

router.post('/Login', (req,res) => {

});*/

router.post('/', (req,res) => {
    const { usuario, nombre, apellido, correo, telefono, direccion, contraseña, rol } = req.body;
    if(usuario && nombre && apellido && correo && telefono && direccion && contraseña && rol)
    {
        if(rol === "Administrador")
        {
            res.json('Usuario agregado con exito');
            const id = users.length + 1;
            const nuevoUsuario = {...req.body, id};
            if(nuevoUsuario.correo != correo)
            {
                users.push(nuevoUsuario);
            }
            else
            {
                res.json('correo repetido');
            }
            res.json(users);
        }
    }
    else
    {
        res.json('No tienes permiso para agregar un usuario');
    }
});

router.put('/:id', (req, res) => {
    const { id } = req.params;
    const { usuario, nombre, apellido, correo, telefono, direccion, contraseña } = req.body;
    if (id && usuario && nombre && apellido && correo && telefono && direccion && contraseña) {
        _.each(users, (user, i) => {
            if (user.id === id) {
                user.usuario = usuario;
                user.nombre = nombre;
                user.apellido = apellido;
                user.correo = correo;
                user.telefono = telefono;
                user.direccion = direccion;
                user.contraseña = contraseña;
            }
        });
        res.json(users);
    } else {
        res.status(500).json({error: 'There was an error.'});
    }
});

router.delete('/:id', (req, res) => {
    const id = req.params;
    if (id) 
    {
        _.each(users, (user, i) => {
            if (user.id === id) {
                users.splice(i, 1);
            }
        });
        res.json(users);
    }
});

module.exports = router;