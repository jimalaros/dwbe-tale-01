const express = require('express');
const { each } = require('underscore');
const router = express.Router();
const _ = require('underscore');

const productos = require('../productos.json');

/*router.get('/Productospordescripcion/:descripcion', (req, res) => {
    const descripcion = req.params.descripcion;
    const _iltro = obtenerProductos().find(u => u.descripcion == descripcion);
    if (filtro) {
        res.json(filtro)
    } else {
        res.status(404).json('Producto no encontrado');
    }
});*/

router.get('/', (req, res) => {
    res.json(productos);
});

router.post('/', (req,res) => {
    const { nombre, precio, descripcion } = req.body;
    if(nombre && precio && descripcion)
    {
        res.json('Producto agregado con exito');
        const id = productos.length + 1;
        const nuevoProducto = {...req.body, id};
        productos.push(nuevoProducto);
        res.json(productos);
    }
    else{
        res.json({error: 'Hubo un error'});
    }
});

router.put('/:id', (req, res) => {
    const { id } = req.params;
    const { nombre, precio, descripcion } = req.body;
    if (id && nombre && precio && descripcion && rating) {
        _.each(productos, (producto, i) => {
            if (producto.id === id) {
                producto.nombre = nombre;
                producto.precio = precio;
                producto.descripcion = year;
            }
        });
        res.json(productos);
    } else {
        res.status(500).json({error: 'There was an error.'});
    }
});

router.delete('/:id', (req, res) => {
    const id = req.params;
    if (id) 
    {
        _.each(productos, (producto, i) => {
            if (producto.id === id) {
                productos.splice(i, 1);
            }
        });
        res.json(productos);
    }
});


module.exports = router;

