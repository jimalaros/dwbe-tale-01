const express = require('express');
const { each } = require('underscore');
const router = express.Router();
const _ = require('underscore');

const productos = require('../productos.json');
const pedidos = [];



router.get('/', (req, res) => {
    res.json(pedidos);
});

router.get('/precio', (req,res) => {
    
    if(pedidos.length>0)
    {
        pedidos.forEach(pedido => {
                
            const item = pedido.item;
            const cantidad = pedido.cantidad;
                
            productos.forEach(producto => {
                const nombre = producto.nombre;
                const precio = producto.precio;

                if(item === nombre)
                {
                    const price = Number(precio);
                    if(cantidad > 0)
                    {
                        precioitem = price*cantidad;
                        res.json(`Usted pidio ${cantidad} ${item}, total del pedido ${precioitem} pesos`) 
                    }
                    else
                    {
                        res.json(`usted pidio 1 ${item}, total del pedido ${price} pesos`) 
                    }
                } 
            });
        });        
    }
    else{
        res.json('el producto no existe');
    }
    
});

router.post('/', (req,res) => {
    const { item, cantidad, mediodepago } = req.body;
    if(item && cantidad && mediodepago)
    {
        res.json('Pedido agregado con exito');
        const id = pedidos.length + 1;
        const nuevoPedido = {  item, cantidad, mediodepago, id };
        pedidos.push(nuevoPedido);
        res.json(pedidos);
    }
    else{
        res.json({error: 'Hubo un error'});
    }
});

router.put('/:id', (req, res) => {
    const { item1, cantidaditem1, item2, cantidaditem2, mediodepago } = req.body;
    if (item && mediodepago) {
        const id = pedidos.length + 1;
        _.each(pedidos, (pedido, i) => {
            if (pedido.id === id) {
                pedido.item1 = item1;
                pedido.item2 = item2;
                pedido.cantidaditem1 = cantidaditem1;
                pedido.cantidaditem2 = cantidaditem2;
                pedido.mediodepago = mediodepago;
            }
        });
        res.json(pedidos);
    } else {
        res.status(500).json({error: 'There was an error.'});
    }
});

router.delete('/item', (req, res) => {
    if (id) 
    {
        _.each(pedidos, (pedido, i) => {
            if (pedido.item === item) {
                pedidos.splice(i, 1);
            }
        });
        res.json(pedidos);
    }
});


module.exports = router;

